from rest_framework import status, views
from django.conf import settings
from rest_framework.response import Response
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.permissions import IsAuthenticated
from chatApp.serializers.userSerializer import UserSerializer
from chatApp.models.user import User

class UserView(views.APIView):

    permission_classes = (IsAuthenticated,)    

    def get(self, request, *args, **kwargs):

        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False) 
        
        serializer = UserSerializer()
        u = User(email = valid_data["user_email"])
        result = serializer.to_representation(u)

        return Response(result, status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)

        request.data["email"] = valid_data["user_email"]
        u = User(email = valid_data["user_email"])

        serializer = UserSerializer(u, data=request.data)        
        serializer.is_valid(raise_exception=True)        
        updatedUser = serializer.save()
        result = serializer.to_representation(updatedUser)

        return Response(result, status=status.HTTP_200_OK)