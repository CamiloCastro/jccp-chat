from django.db import models
from .user import User

class Chat(models.Model):
    id = models.AutoField(primary_key=True)    
    name = models.CharField('Name', max_length = 256, null=False, default="CHAT")
    color = models.CharField('Color', max_length = 256, null=False, default="Blanco")
    creationDate = models.DateTimeField()    