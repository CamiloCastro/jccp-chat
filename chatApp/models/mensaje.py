from django.db import models
from .user import User
from .chat import Chat

class Mensaje(models.Model):    
    id = models.AutoField(primary_key=True)    
    user = models.ForeignKey(User, related_name="mensaje", on_delete=models.CASCADE)
    chat = models.ForeignKey(Chat, related_name="mensaje", on_delete=models.CASCADE)
    text = models.TextField('Text', null=False, default="")
    creationDate = models.DateTimeField()