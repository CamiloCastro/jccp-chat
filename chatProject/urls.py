"""chatProject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from chatApp import views
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)

urlpatterns = [
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    path('register/', views.UserCreateView.as_view()),
    path('user/', views.UserView.as_view()),
    path('chat/', views.ChatView.as_view()),
    path('chat/<int:idChat>/', views.ChatView.as_view()),
    path('integrante/', views.IntegranteView.as_view()),
    path('integrante/<int:id>/', views.IntegranteView.as_view()),
    path('mensaje/', views.MensajeView.as_view()),
    path('mensaje/<int:id>/', views.MensajeView.as_view()),    
]
