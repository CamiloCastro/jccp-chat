from django.conf import settings
from rest_framework import status, views
from rest_framework.response import Response
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.permissions import IsAuthenticated
from chatApp.models.chat import Chat

from chatApp.serializers.chatSerializer import ChatSerializer
from chatApp.serializers.integranteSerializer import IntegranteSerializer


class ChatView(views.APIView):

    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):

        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)        
        
        chSerializer = ChatSerializer(data=request.data)        
        chSerializer.is_valid(raise_exception=True)        
        chatInstance = chSerializer.save()
        jsonChatInstance = chSerializer.to_representation(chatInstance)

        integranteData = {
            "chat" : chatInstance.id,
            "isAdmin" : True,
            "user" : valid_data["user_email"]
        }
        intSerializer = IntegranteSerializer(data = integranteData)
        intSerializer.is_valid(raise_exception=True)
        intSerializer.save()

        return Response(jsonChatInstance, status=status.HTTP_201_CREATED)

    def get(self, request, *args, **kwargs):

        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)  

        chSerializer = ChatSerializer()
        intSerializer = IntegranteSerializer()

        if "idChat" in kwargs:
            
            query_result = intSerializer.get_element(user=valid_data["user_email"], chat=kwargs["idChat"])

            if query_result.count() == 0:
                stringResponse = {'detail':'No está autorizado para ver la información del chat'}
                return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)

            ch = Chat(id=kwargs["idChat"])
            result = chSerializer.to_representation(ch)
            return Response(result, status=status.HTTP_200_OK)

        else:

            query_result = intSerializer.get_element(user=valid_data["user_email"])
            result = []
            for integrante in query_result:
                ch = chSerializer.to_representation(integrante.chat)
                result.append(ch)
            
            return Response(result, status=status.HTTP_200_OK)


