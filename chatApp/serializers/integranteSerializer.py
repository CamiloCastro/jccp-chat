from rest_framework import serializers
from chatApp.models.integrante import Integrante

class IntegranteSerializer(serializers.ModelSerializer):    
    
    
    class Meta:
        
        model = Integrante
        fields = ['user', 'chat', 'isAdmin'] 
         
    
    def get_element(self, **obj):        
        return Integrante.objects.filter(**obj)

    def delete_element(self, id):
        men = Integrante.objects.get(pk=id)
        men.delete()
        return men
    