from rest_framework import serializers
from chatApp.models.user import User

class UserSerializer(serializers.ModelSerializer):    
    
    class Meta:
        
        model = User
        fields = ['email', 'password', 'name', 'status']

    def to_representation(self, obj):

         userInstance = User.objects.get(email=obj.email)   

         return {
             "email" : userInstance.email,
             "name" : userInstance.name,
             "status" : userInstance.status
         }


    