from django.utils import timezone
import datetime
from rest_framework import serializers
from chatApp.models.chat import Chat

class ChatSerializer(serializers.ModelSerializer):
    
    class Meta:
        
        model = Chat
        fields = ['id', 'name', 'color']
    
    def create(self, validated_data):
        chatInstance = Chat.objects.create(creationDate = datetime.datetime.now(tz=timezone.utc), **validated_data)        
        return chatInstance
    
    def to_representation(self, obj):
        chat = Chat.objects.get(id=obj.id)          
        return {
            "id" : chat.id,
            "name" : chat.name,
            "color" : chat.color,
            "creationDate" : chat.creationDate
        }
    