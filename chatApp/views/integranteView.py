from django.conf import settings
from rest_framework import status, views
from rest_framework.response import Response
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.permissions import IsAuthenticated

from chatApp.serializers.chatSerializer import ChatSerializer
from chatApp.serializers.integranteSerializer import IntegranteSerializer
from chatApp.serializers.userSerializer import UserSerializer


class IntegranteView(views.APIView):

    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):

        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)      
        intSerializer = IntegranteSerializer(data = request.data)

        result = intSerializer.get_element(user=valid_data["user_email"], isAdmin=True, chat=request.data["chat"])

        if result.count() == 0:
            stringResponse = {'detail':'No está autorizado para agregar un integrante al chat'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)

        result = intSerializer.get_element(user=request.data["user"], chat=request.data["chat"])

        if result.count() > 0:
            stringResponse = {'detail':'El usuario ya está agregado en el chat'}
            return Response(stringResponse, status=status.HTTP_400_BAD_REQUEST)      

        intSerializer.is_valid(raise_exception=True)
        intSerializer.save()

        return Response({"detail" : "El usuario fue agregado exitosamente al chat"}, status=status.HTTP_201_CREATED)

    def get(self, request, *args, **kwargs):

        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)

        intSerializer = IntegranteSerializer()
        result = intSerializer.get_element(user=valid_data["user_email"], chat=kwargs["id"])

        if result.count() == 0:
            stringResponse = {'detail':'No está autorizado para ver los mensajes del chat'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        
        userSerializer = UserSerializer()
        query_result = intSerializer.get_element(chat=kwargs["id"])

        result = []
        for m in query_result:
            data = userSerializer.to_representation(m.user)
            data["isAdmin"] = m.isAdmin
            data["id"] = m.id
            result.append(data)
        
        return Response(result, status=status.HTTP_200_OK)

    def delete(self, request, *args, **kwargs):

        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)

        intSerializer = IntegranteSerializer()        
        query_result = intSerializer.get_element(id = kwargs["id"])

        if query_result.count() == 0:
            stringResponse = {'detail':'No está autorizado para eliminar el integrante'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        
        intEliminar = query_result[0]

        query_result = intSerializer.get_element(user=valid_data["user_email"], chat=intEliminar.chat, isAdmin=True)

        if query_result.count() == 0:
            stringResponse = {'detail':'No está autorizado para eliminar el integrante'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        

        deletedInt = intSerializer.delete_element(kwargs["id"])
        return Response(intSerializer.to_representation(deletedInt), status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):

        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)

        intSerializer = IntegranteSerializer()        
        query_result = intSerializer.get_element(id = kwargs["id"])

        if query_result.count() == 0:
            stringResponse = {'detail':'No está autorizado para cambiar el rol del integrante'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        
        intActualizar = query_result[0]

        query_result = intSerializer.get_element(user=valid_data["user_email"], chat=intActualizar.chat, isAdmin=True)

        if query_result.count() == 0:
            stringResponse = {'detail':'No está autorizado para cambiar el rol del integrante'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)                

        ser = IntegranteSerializer(intActualizar, data=request.data)
        ser.is_valid(raise_exception=True)        
        updatedInt = ser.save()        

        return Response(intSerializer.to_representation(updatedInt), status=status.HTTP_200_OK)