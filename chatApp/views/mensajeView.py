from django.conf import settings
from rest_framework import status, views
from rest_framework.response import Response
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.permissions import IsAuthenticated

from chatApp.serializers.mensajeSerializer import MensajeSerializer
from chatApp.serializers.chatSerializer import ChatSerializer
from chatApp.serializers.integranteSerializer import IntegranteSerializer


class MensajeView(views.APIView):

    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):

        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)

        request.data["user"] = valid_data["user_email"]

        intSerializer = IntegranteSerializer()
        result = intSerializer.get_element(user=valid_data["user_email"], chat=request.data["chat"])

        if result.count() == 0:
            stringResponse = {'detail':'No está autorizado para agregar un mensaje al chat'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        
        menSerializer = MensajeSerializer(data = request.data)
        menSerializer.is_valid(raise_exception=True)
        menSerializer.save()

        return Response({"detail" : "El mensaje fue agregado exitosamente al chat"}, status=status.HTTP_201_CREATED)

    def get(self, request, *args, **kwargs):

        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)

        intSerializer = IntegranteSerializer()
        result = intSerializer.get_element(user=valid_data["user_email"], chat=kwargs["id"])

        if result.count() == 0:
            stringResponse = {'detail':'No está autorizado para ver los mensajes del chat'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)

        menSerializer = MensajeSerializer()
        query_result = menSerializer.get_element(chat=kwargs["id"])

        result = []
        for m in query_result:
            result.append(menSerializer.to_representation(m))
        
        return Response(result, status=status.HTTP_200_OK)

    def delete(self, request, *args, **kwargs):

        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)

        menSerializer = MensajeSerializer()        
        query_result = menSerializer.get_element(id=kwargs["id"], user=valid_data["user_email"])
        if query_result.count() == 0:
            stringResponse = {'detail':'No está autorizado para eliminar el mensaje'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        

        deletedMen = menSerializer.delete_element(kwargs["id"])

        return Response(menSerializer.to_representation(deletedMen), status=status.HTTP_200_OK)




