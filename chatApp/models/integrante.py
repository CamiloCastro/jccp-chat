from django.db import models
from .user import User
from .chat import Chat

class Integrante(models.Model):    
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, related_name="integrante", on_delete=models.CASCADE)
    chat = models.ForeignKey(Chat, related_name="integrante", on_delete=models.CASCADE)
    isAdmin = models.BooleanField(default=False)    