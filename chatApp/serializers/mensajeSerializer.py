from django.utils import timezone
import datetime
from rest_framework import serializers
from chatApp.models.mensaje import Mensaje

class MensajeSerializer(serializers.ModelSerializer):        
    
    class Meta:
        
        model = Mensaje
        fields = ['id', 'user', 'chat', 'text'] 

    def create(self, validated_data):
        chatInstance = Mensaje.objects.create(creationDate = datetime.datetime.now(tz=timezone.utc), **validated_data)        
        return chatInstance 
    
    def get_element(self, **obj):        
        return Mensaje.objects.filter(**obj)
    
    def delete_element(self, id):
        men = Mensaje.objects.get(pk=id)
        men.delete()
        return men
